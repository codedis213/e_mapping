# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class DmozItem(scrapy.Item):
    plink = scrapy.Field()
    ptype = scrapy.Field()
    title = scrapy.Field()
    brand = scrapy.Field()
    sub_title = scrapy.Field()
    pro_img = scrapy.Field()
    prise = scrapy.Field()
    seller_link = scrapy.Field()
    seller_name = scrapy.Field()
    spec_dict = scrapy.Field()