import scrapy

from tutorial.items import DmozItem
from bs4 import BeautifulSoup
import re
import json

class DmozSpider(scrapy.Spider):
    name = "flipkart"
    allowed_domains = ["flipkart.com"]

    start_urls = [
        "http://www.flipkart.com/mobiles/pr?sid=tyy,4io&otracker=ch_vn_mobile_filter_Top%20Brands_All",
        # "http://www.flipkart.com/htc-desire-620g-dual-sim/p/itme7zfwbafqacj3?pid=MOBE2ZFHHKG8YUGQ&al=FyMviNFNIO0X%2FuxV9m7lLMldugMWZuE7Qdj0IGOOVqvqZNhW%2FnyyYqY8d6DznOyE5ezqSUsmWcY%3D&ref=L%3A5098729725858893707&srno=b_81&findingMethod=ch_vn_mobile_filter",
    ]


    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        searchCount = soup.find("div",{"id":"searchCount"})
        items_count = searchCount.find_all("span", **{"class":"items"})

        if items_count:
            count_string = items_count[0].get_text()
            count_string = int(str(count_string.replace(",", "")).strip())

            # href_link_list = ["http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=" +str(x)+ "&ajax=true"
            #                   for x in range(0, count_string, 20)]

            href_link_list = ["http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?p%5B%5D=facets.availability%255B%255D%3DExclude%2BOut%2Bof%2BStock&sid=tyy%2C4io&filterNone=true&start="+str(x)+"&ajax=true"
                              for x in range(0, count_string, 20)]

            for url in href_link_list:
                yield scrapy.Request(url, callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        a_tag_list = soup2.find_all("a", {"data-tracking-id":"prd_title"})

        product_link_list = ["http://www.flipkart.com%s" % a_tag.get("href") for a_tag in a_tag_list]

        for url in product_link_list:
                yield scrapy.Request(url, callback=self.parse_pro_link)

    def parse_pro_link(self, response):
        item = DmozItem()
        soup3 = BeautifulSoup(response.body, "html.parser")
        item["plink"] = response.url
        item["ptype"] = "mobile"
        item["title"] = soup3.find("h1", {"itemprop":"name"}).get_text()

        item["sub_title"] = ''
        sub_title = soup3.find("span", {"class":"subtitle"})
        if sub_title:
            item["sub_title"] = sub_title.get_text()

        img_list = soup3.find("div", {"class":"mainImage"}).find_all("img", {"class":re.compile("productImage")})
        item["pro_img"] = json.dumps([img.get("data-src") for img in img_list])
        item["prise"] = soup3.find("span", {"class":"selling-price omniture-field"}).get_text()
        seller_a = soup3.find("a", {"class":"seller-name"})
        item["seller_link"] = seller_a.get("href")
        item["seller_name"] = seller_a.get_text()
        spec_div = soup3.find("div", {"class":"productSpecs specSection"})
        specTable_list = spec_div.find_all("table", {"class":"specTable"})

        spec_dict = {}
        item["brand"] = ''

        for specTable in specTable_list:
            all_tr = specTable.find_all("tr")
            heading = all_tr[0].find("th", {"class":"groupHead"}).get_text()

            spec_dict[str(heading).strip()]={}



            for tr in all_tr[1:]:
                specsKey = tr.find("td", {"class":"specsKey"}).get_text()
                specsValue = tr.find("td", {"class":"specsValue"}).get_text()
                if str(specsKey).strip() == "Brand":
                    item["brand"] = str(specsValue).strip()

                spec_dict[heading][str(specsKey).strip()] = str(specsValue).strip()

        item["spec_dict"] = json.dumps(spec_dict)

        yield item
