from django.contrib import admin

# Register your models here.
from e_map_app.models import EMapCollection

admin.site.register(EMapCollection)
