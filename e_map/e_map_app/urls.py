from django.conf.urls import patterns, include, url


urlpatterns = patterns('e_map_app.views',
    url(r'^$', 'home', name="home"),
    url(r'preview/$', 'preview', name="preview"),
    url(r'contact/$', 'contact', name="contact"),

)
