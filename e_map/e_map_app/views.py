from django.shortcuts import render

from django.shortcuts import render
from models import EMapCollection


# def home(request):
    # for emap in EMapCollection.objects:
    #     print emap.spec_dict["GENERAL FEATURES"]["Brand"]

    # emap_obj_list = EMapCollection.objects.aggregate({"$group": {"_id": "$title",
    #                                                              "title": {"$first": '$title'},
    #                                                              "spec_dict": {"$first": "$spec_dict"},
    #                                                              "ptype": {"$first": "$ptype"},
    #                                                              "sub_title": {"$first": "$sub_title"},
    #                                                              "pro_img": {"$first": "$pro_img"},
    #                                                              "prise": {"$first": "$prise"},
    #                                                              "seller_link": {"$first": "$seller_link"},
    #                                                              "seller_name": {"$first": "$seller_name"}
    #                                                              }
    #                                                   },
    #                                                  {"$sort": {"spec_dict.GENERAL FEATURES.Brand": -1 }}
    #                                                  )

    # return render(request, 'e_map_app/index.html', {"emap_obj_list":emap_obj_list})

def home(request):

    emap_obj_list = EMapCollection.objects.order_by('brand')
    brands_list = emap_obj_list.values("brand").distinct()
    emap_obj_list = emap_obj_list.all()[:5]

    context = {"emap_obj_list": emap_obj_list,
               "brands_list": brands_list}

    return render(request, 'e_map_app/index.html', context)


def preview(request):
    return render(request, 'e_map_app/preview.html', {})

def contact(request):
    return render(request, 'e_map_app/contact.html', {})


