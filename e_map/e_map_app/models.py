from django.db import models
import json

# Create your models here.

# from mongoengine import *
# from e_map.settings import MONGODB_DB, MONGODB_COLLECTION
#
# connect(MONGODB_DB)
#
# class EMapCollection(Document):
#     ptype = StringField(max_length=120, required=True)
#     title = StringField(max_length=120, required=True)
#     sub_title = StringField(max_length=120, required=False)
#     pro_img = ListField(URLField(max_length=30))
#     prise = StringField(required=False)
#     seller_link = URLField(max_length=30, required=False)
#     seller_name = StringField(max_length=120, required=True)
#     spec_dict = DictField(required=True)
#
#
#     meta = {
#         'collection': MONGODB_COLLECTION,
#         'ordering': ['prise'],
#         'unique': True,
#     }
#
#     def __str__(self):
#         return "%s %s" % (self.title, self.sub_title)


class EMapCollection(models.Model):
    ptype = models.CharField(max_length=50, blank=True, verbose_name="ptype")
    plink = models.URLField(verbose_name="plink")
    brand = models.CharField(max_length=50, verbose_name="brand")
    title = models.CharField(max_length=100, verbose_name="title")
    sub_title = models.CharField(max_length=100, verbose_name="sub_title")
    pro_img = models.TextField(verbose_name="pro_img")
    prise = models.CharField(max_length=30, verbose_name="prise")
    seller_link = models.URLField(blank=False, verbose_name="seller_link")
    seller_name = models.CharField(max_length=50, blank=False, verbose_name="seller_name")
    spec_dict = models.TextField(verbose_name="spec_dict")

    def set_pro_img(self, x):
        self.pro_img = json.dumps(x)

    def get_pro_img(self):
        return json.loads(self.pro_img)

    def set_spec_dict(self, x):
        self.pro_img = json.dumps(x)

    def get_spec_dict(self):
        return json.loads(self.spec_dict)

    def __str__(self):
        return "%s %s" %(self.title, self.sub_title)

    class Meta:
        db_table = 'e_map_collection'
        ordering = ['-brand']
        unique_together = ("title", "sub_title")