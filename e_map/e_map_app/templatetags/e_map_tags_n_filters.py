from django import template
register = template.Library()

@register.filter(name='getdictkey')
def getdictkey(value, arg):
    return value.get(arg, [])