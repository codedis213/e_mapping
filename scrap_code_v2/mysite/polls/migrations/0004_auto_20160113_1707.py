# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_dmozitem_domain'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dmozitem',
            name='additional_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='audio_jack',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='availability',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='battery_type',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='bluetooth',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='brand',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='business_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='call_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='call_memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='color',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='edeg',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='expandable_memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='flash',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='fm',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='form',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='fourg',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='front_facing_camera',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='gprs',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='graphics',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='handset_color',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='hd_recording',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='important_apps',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='in_the_box',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='internal',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='internet_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='model_id',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='model_name',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='music_player',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='name',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='navigation_technology',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='nfc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='operating_freq',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='os',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='other_camera_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='other_colors',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='other_display_features',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='phone_book_memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='preinstalled_browser',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='price',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='processor',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='product_emi',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='rating',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='rear_camera',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='removable_battery',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='resolution',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sar_value',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sensors',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sim_size',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sim_type',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='size',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sms_memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sound_enhancement',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='sub_name',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='tethering',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='threeg',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='touch_screen',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='usb_connectivity',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='user_interface',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='user_memory',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='video_calling',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='video_player',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='video_recording',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='warranty_summary',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='wifi',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dmozitem',
            name='zoom',
            field=models.TextField(null=True, blank=True),
        ),
    ]
