# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20151223_1846'),
    ]

    operations = [
        migrations.AddField(
            model_name='dmozitem',
            name='domain',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
