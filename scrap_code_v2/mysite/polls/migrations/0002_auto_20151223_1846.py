# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dmozitem',
            name='other_camera_features',
            field=models.TextField(max_length=100, null=True, blank=True),
        ),
    ]
