from django.contrib import admin


from .models import DmozItem, ComparerajaMobile

admin.site.register(DmozItem)
admin.site.register(ComparerajaMobile)
