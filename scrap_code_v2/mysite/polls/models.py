from django.db import models

# Create your models here.
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


class DmozItem(models.Model):
    domain = models.CharField(max_length=100, blank=True, null=True)
    product_type = models.CharField(max_length=100, blank=True, null=True)
    product_link = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    sub_name = models.TextField(blank=True, null=True)
    rating = models.TextField(blank=True, null=True)
    image_list = models.TextField( blank=True, null=True)
    price = models.TextField(blank=True, null=True)
    product_emi = models.TextField(blank=True, null=True)
    availability = models.TextField(blank=True, null=True)
    other_colors = models.TextField(blank=True, null=True)
    in_the_box = models.TextField(blank=True, null=True)
    # General features
    brand = models.TextField(blank=True, null=True)
    handset_color = models.TextField(blank=True, null=True)
    form = models.TextField(blank=True, null=True)
    sim_size = models.TextField(blank=True, null=True)
    call_features = models.TextField(blank=True, null=True)
    model_name = models.TextField(blank=True, null=True)
    touch_screen = models.TextField(blank=True, null=True)
    sim_type = models.TextField(blank=True, null=True)
    model_id = models.TextField(blank=True, null=True)
    video_calling = models.TextField(blank=True, null=True)
    # Multimedia
    sound_enhancement = models.TextField(blank=True, null=True)
    video_player = models.TextField(blank=True, null=True)
    fm  = models.TextField(blank=True, null=True)
    music_player = models.TextField(blank=True, null=True)
    # Camera
    video_recording = models.TextField(blank=True, null=True)
    flash = models.TextField(blank=True, null=True)
    other_camera_features = models.TextField(blank=True, null=True)
    hd_recording = models.TextField(blank=True, null=True)
    rear_camera = models.TextField(blank=True, null=True)
    front_facing_camera = models.TextField(blank=True, null=True)
    zoom = models.TextField(blank=True, null=True)
    # Internet & connectivity
    audio_jack = models.TextField(blank=True, null=True)
    preinstalled_browser = models.TextField(blank=True, null=True)
    bluetooth = models.TextField(blank=True, null=True)
    fourg = models.TextField(blank=True, null=True)
    navigation_technology = models.TextField(blank=True, null=True)
    wifi = models.TextField(blank=True, null=True)
    internet_features = models.TextField(blank=True, null=True)
    edeg = models.TextField(blank=True, null=True)
    gprs = models.TextField(blank=True, null=True)
    tethering = models.TextField(blank=True, null=True)
    usb_connectivity = models.TextField(blank=True, null=True)
    threeg = models.TextField(blank=True, null=True)
    # Other features
    sensors = models.TextField(blank=True, null=True)
    call_memory = models.TextField(blank=True, null=True)
    sar_value = models.TextField(blank=True, null=True)
    important_apps = models.TextField(blank=True, null=True)
    additional_features = models.TextField(blank=True, null=True)
    phone_book_memory = models.TextField(blank=True, null=True)
    sms_memory = models.TextField(blank=True, null=True)
    # Dimensions
    # weight = models.CharField(max_length=100, blank=True, null=True)
    # dimensions_size = models.CharField(max_length=100, blank=True, null=True)
    dimensions = models.TextField(blank=True, null=True)
    # Display
    resolution = models.TextField(blank=True, null=True)
    other_display_features = models.TextField(blank=True, null=True)
    color = models.TextField(blank=True, null=True)
    size = models.TextField(blank=True, null=True)
    # Warranty
    warranty_summary = models.TextField(blank=True, null=True)
    # Battery
    removable_battery = models.TextField(blank=True, null=True)
    battery_type = models.TextField(blank=True, null=True)
    # Memory and storage
    expandable_memory = models.TextField(blank=True, null=True)
    memory = models.TextField(blank=True, null=True)
    internal = models.TextField(blank=True, null=True)
    user_memory = models.TextField(blank=True, null=True)
    # Platform
    operating_freq = models.TextField(blank=True, null=True)
    user_interface = models.TextField(blank=True, null=True)
    graphics = models.TextField(blank=True, null=True)
    os = models.TextField(blank=True, null=True)
    processor = models.TextField(blank=True, null=True)
    business_features = models.TextField(blank=True, null=True)
    nfc = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "%s %s" %(self.name, self.sub_name)


class ComparerajaMobile(models.Model):
    error_link = models.CharField(max_length=10, blank=True, null=True)
    article_link = models.TextField(blank=True, null=True)
    product_name = models.CharField(max_length=100, blank=True, null=True)
    product_sku = models.CharField(max_length=15, blank=True, null=True)
    product_buy_link = models.TextField(blank=True, null=True)
    product_domain_img = models.TextField(blank=True, null=True)
    product_price = models.CharField(max_length=15, blank=True, null=True)
    product_score = models.CharField(max_length=15, blank=True, null=True)
    product_img_list = models.TextField(blank=True, null=True)
    Network = models.CharField(max_length=15, blank=True, null=True)
    MicroSIM = models.CharField(max_length=15, blank=True, null=True)
    NanoSIM = models.CharField(max_length=15, blank=True, null=True)
    DualSIM = models.CharField(max_length=15, blank=True, null=True)
    Touchscreen = models.CharField(max_length=15, blank=True, null=True)
    Dimension = models.CharField(max_length=50, blank=True, null=True)
    Weight = models.CharField(max_length=20, blank=True, null=True)
    Type = models.CharField(max_length=20, blank=True, null=True)
    ScreenSize = models.CharField(max_length=15, blank=True, null=True)
    ScreenResolution = models.CharField(max_length=50, blank=True, null=True)
    DisplayType = models.CharField(max_length=15, blank=True, null=True)
    InternalMemory = models.CharField(max_length=20, blank=True, null=True)
    ExternalMemory = models.CharField(max_length=20, blank=True, null=True)
    Ram = models.CharField(max_length=10, blank=True, null=True)
    PrimaryCamera = models.CharField(max_length=10, blank=True, null=True)
    SecondaryCamera = models.CharField(max_length=10, blank=True, null=True)
    PrimaryVideoQuality = models.CharField(max_length=10, blank=True, null=True)
    SecondaryVideoQuality = models.CharField(max_length=10, blank=True, null=True)
    Flash = models.CharField(max_length=10, blank=True, null=True)
    WIFI = models.CharField(max_length=10, blank=True, null=True)
    Radio = models.CharField(max_length=10, blank=True, null=True)
    GPRS = models.CharField(max_length=10, blank=True, null=True)
    EDGE = models.CharField(max_length=10, blank=True, null=True)
    NFC = models.CharField(max_length=10, blank=True, null=True)
    USB = models.CharField(max_length=10, blank=True, null=True)
    BluetoothVersion = models.CharField(max_length=10, blank=True, null=True)
    OS = models.CharField(max_length=25, blank=True, null=True)
    OSVersion = models.CharField(max_length=10, blank=True, null=True)
    ProcessorType = models.CharField(max_length=50, blank=True, null=True)
    ProcessorSpeed = models.CharField(max_length=50, blank=True, null=True)
    Chipset = models.CharField(max_length=50, blank=True, null=True)
    GPU = models.CharField(max_length=50, blank=True, null=True)
    BatteryType = models.CharField(max_length=50, blank=True, null=True)
    BatteryCapacity = models.CharField(max_length=50, blank=True, null=True)
    mmJack = models.CharField(max_length=50, blank=True, null=True)
    Loudspeaker = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.product_name


