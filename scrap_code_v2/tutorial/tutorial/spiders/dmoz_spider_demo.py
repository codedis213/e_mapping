import scrapy

from tutorial.items import DmozItem
from bs4 import BeautifulSoup
import re
import json

class DmozSpider(scrapy.Spider):
    name = "flipkart_demo"
    allowed_domains = ["flipkart.com"]

    start_urls = [
        "http://www.flipkart.com/moto-x-play/p/itmeajtqyhrnkwrz?pid=MOBEAJTQGSBHEFHA&al=%2BbUjNsVCEnprtYCxLB%2FWxcldugMWZuE7Qdj0IGOOVqvmU2ZqdKxAnwpk%2BxH53aGM9Xb7x5fMYT4%3D&ref=L%3A-4668056859870149875&srno=b_40",
    ]

    def parse(self, response):
        item = DmozItem()
        soup = BeautifulSoup(response.body, "html.parser")
        item["domain"] = "http://www.flipkart.com"
        item["product_type"] = "mobile"
        item["product_link"] = response.url
        try:
            item["name"] = soup.find("h1", {"itemprop":"name"}).get_text()
        except:
            item["name"] =''

        try:
            item["sub_name"] = soup.find("span", {"class":"subtitle"}).get_text()
        except:
            item["sub_name"] =''

        item["rating"] = ''
        try:
            img_list = soup.find_all("img", attrs={"class":"productImage"})
            item["image_list"] = [img.get("data-zoomimage") for img in img_list]
        except:
            item["image_list"] =''

        try:
            product_selling_prce = soup.find("span", {"class":"selling-price omniture-field"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] =''

        try:
            product_emi = soup.find("span", {"class":"emi-text"}).get_text()
            item["product_emi"] = "".join([ch for ch in product_emi  if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["product_emi"] =''

        try:
            item["availability"] = soup.find("td", text=re.compile("Availability")).find_next("td").get_text().strip()
        except:
            item["availability"] =''

        try:
            item["other_colors"] = soup.find("td", text=re.compile("Other colors")).find_next("td").get_text().strip()
        except:
            item["other_colors"] =''

        try:
            item["in_the_box"] = soup.find("td", text=re.compile("In the box")).find_next("td").get_text().strip()
        except:
            item["in_the_box"] =''

        try:
            item["brand"] = soup.find("td", text=re.compile("Brand")).find_next("td").get_text().strip()
        except:
            item["brand"] =''

        try:
            item["handset_color"] = soup.find("td", text=re.compile("Handset Color")).find_next("td").get_text().strip()
        except:
            item["handset_color"] =''

        try:
            item["form"] = soup.find("td", text=re.compile("Form")).find_next("td").get_text().strip()
        except:
            item["form"] =''

        try:
            item["sim_size"] = soup.find("td", text=re.compile("SIM Size")).find_next("td").get_text().strip()
        except:
            item["sim_size"] =''

        try:
            item["call_features"] = soup.find("td", text=re.compile("Call Features")).find_next("td").get_text().strip()
        except:
            item["call_features"] =''

        try:
            item["model_name"] = soup.find("td", text=re.compile("Model Name")).find_next("td").get_text().strip()
        except:
            item["model_name"] =''

        try:
            item["touch_screen"] = soup.find("td", text=re.compile("Touch Screen")).find_next("td").get_text().strip()
        except:
            item["touch_screen"] =''

        try:
            item["sim_type"] = soup.find("td", text=re.compile("SIM Type")).find_next("td").get_text().strip()
        except:
            item["sim_type"] =''

        try:
            item["model_id"] = soup.find("td", text=re.compile("Model ID")).find_next_sibling("td").get_text().strip()
        except:
            item["model_id"] =''

        try:
            item["video_calling"] = soup.find("td", text=re.compile("Video Calling")).find_next_sibling("td").get_text().strip()
        except:
            item["video_calling"] =''

        try:
            item["sound_enhancement"] = soup.find("td", text=re.compile("Sound Enhancement")).find_next_sibling("td").get_text().strip()
        except:
            item["sound_enhancement"] =''

        try:
            item["video_player"] = soup.find("td", text=re.compile("Video Player")).find_next_sibling("td").get_text().strip()
        except:
            item["video_player"] =''

        try:
            item["fm"] = soup.find("td", text=re.compile("FM")).find_next_sibling("td").get_text().strip()
        except:
             item["fm"] =''

        try:
            item["music_player"] = soup.find("td", text=re.compile("Music Player")).find_next_sibling("td").get_text().strip()
        except:
            item["music_player"] =''

        try:
            item["video_recording"] = soup.find("td", text=re.compile("Video Recording")).find_next_sibling("td").get_text().strip()
        except:
            item["video_recording"] =''


        try:
            item["flash"] = soup.find("td", text=re.compile("Flash")).find_next_sibling("td").get_text().strip()
        except:
            item["flash"] =''

        try:
            item["other_camera_features"] = soup.find("td", text=re.compile("Other Camera Features")).find_next_sibling("td").get_text().strip()
        except:
            item["other_camera_features"] =''

        try:
            item["hd_recording"] = soup.find("td", text=re.compile("HD Recording")).find_next_sibling("td").get_text().strip()

        except:
            item["hd_recording"]  =''

        try:
            item["rear_camera"] = soup.find("td", text=re.compile("Rear Camera")).find_next_sibling("td").get_text().strip()
        except:
            item["rear_camera"] = ''

        try:
            item["front_facing_camera"] = soup.find("td", text=re.compile("Front Facing Camera")).find_next_sibling("td").get_text().strip()
        except:
            item["front_facing_camera"] =''

        try:
            item["zoom"] = soup.find("td", text=re.compile("Zoom")).find_next_sibling("td").get_text().strip()
        except:
            item["zoom"] =''

        try:
            item["audio_jack"] = soup.find("td", text=re.compile("Audio Jack")).find_next_sibling("td").get_text().strip()
        except:
            item["audio_jack"] =''

        try:
            item["preinstalled_browser"] = soup.find("td", text=re.compile("Preinstalled Browser")).find_next_sibling("td").get_text().strip()
        except:
            item["preinstalled_browser"] =''

        try:
            item["bluetooth"] = soup.find("td", text=re.compile("Bluetooth")).find_next_sibling("td").get_text().strip()
        except:
            item["bluetooth"] =''

        try:
            item["fourg"]=soup.find("td", text=re.compile("4G")).find_next_sibling("td").get_text().strip()
        except:
            item["fourg"] =''

        try:
            item["navigation_technology"]=soup.find("td", text=re.compile("Navigation Technology")).find_next_sibling("td").get_text().strip()
        except:
            item["navigation_technology"] =''

        try:
            item["wifi"] = soup.find("td", text=re.compile("Wifi")).find_next_sibling("td").get_text().strip()
        except:
            item["wifi"] =''

        try:
            item["internet_features"] = soup.find("td", text=re.compile("Internet Features")).find_next_sibling("td").get_text().strip()
        except:
            item["internet_features"] =''

        try:
            item["edeg"] = soup.find("td", text=re.compile("EDGE")).find_next_sibling("td").get_text().strip()
        except:
            item["edeg"] =''

        try:
            item["gprs"] = soup.find("td", text=re.compile("GPRS")).find_next_sibling("td").get_text().strip()
        except:
            item["gprs"] =''

        try:
            item["tethering"] = soup.find("td", text=re.compile("Tethering")).find_next_sibling("td").get_text().strip()
        except:
            item["tethering"] = ''

        try:
            item["usb_connectivity"] = soup.find("td", text=re.compile("USB Connectivity")).find_next_sibling("td").get_text().strip()
        except:
            item["usb_connectivity"] = ''

        try:
            item["threeg"]=soup.find("td", text=re.compile("3G")).find_next_sibling("td").get_text().strip()
        except:
            item["threeg"] =''

        try:
            item["sensors"] = soup.find("td", text=re.compile("Sensors")).find_next_sibling("td").get_text().strip()
        except:
            item["sensors"] =''

        try:
            item["call_memory"] = soup.find("td", text=re.compile("Call Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["call_memory"]  =''

        try:
            item["sar_value"] = soup.find("td", text=re.compile("SAR Value")).find_next_sibling("td").get_text().strip()
        except:
            item["sar_value"] =''

        try:
            item["important_apps"] = soup.find("td", text=re.compile("Important Apps")).find_next_sibling("td").get_text().strip()
        except:
            item["important_apps"] =''

        try:
            item["additional_features"] = soup.find("td", text=re.compile("Important Apps")).find_next_sibling("td").get_text().strip()
        except:
            item["additional_features"] =''

        try:
            item["phone_book_memory"] = soup.find("td", text=re.compile("Phone Book Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["phone_book_memory"] =''

        try:
            tr = soup.find("th", text=re.compile("Dimensions")).find_parent("tr").find_next_sibling("tr")
            dimensions_str = ''
            while tr:
                dimensions_str += " ".join(str(tr.get_text()).split()).strip()
                tr = tr.find_next_sibling("tr")
            item["dimensions"] = dimensions_str
        except:
            item["dimensions"] = ''

        # item["weight"] = soup.find("td", text=re.compile("Weight")).find_next_sibling("td").get_text().strip()
        # item["dimensions_size"] = soup.find("td", text=re.compile("Size")).find_next_sibling("td").get_text().strip()


        try:
            item["sms_memory"] = soup.find("td", text=re.compile("SMS Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["sms_memory"] =''

        try:
            item["resolution"] = soup.find("td", text=re.compile("Resolution")).find_next_sibling("td").get_text().strip()
        except:
            item["resolution"] =''

        try:
            item["other_display_features"] = soup.find("td", text=re.compile("Other Display Features")).find_next_sibling("td").get_text().strip()
        except:
            item["other_display_features"] =''

        try:
            item["color"] = soup.find("td", text=re.compile("Color")).find_next_sibling("td").get_text().strip()
        except:
            item["color"] =''

        try:
            item["size"] = soup.find("th", text=re.compile("Display")).find("td", text=re.compile("Size")).find_next_sibling("td").get_text().strip()
        except:
            item["size"] =''

        try:
            item["warranty_summary"] = soup.find("td", text=re.compile("Warranty Summary")).find_next_sibling("td").get_text().strip()
        except:
            item["warranty_summary"]  =''

        try:
            item["removable_battery"] = soup.find("td", text=re.compile("Removable Battery")).find_next_sibling("td").get_text().strip()
        except:
            item["removable_battery"] =''

        try:
            item["battery_type"] = soup.find("td", text=re.compile("Type")).find_next_sibling("td").get_text().strip()
        except:
            item["battery_type"] =''

        try:
            item["expandable_memory"] = soup.find("td", text=re.compile("Expandable Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["expandable_memory"] =''

        try:
            item["memory"] = soup.find("td", text=re.compile("Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["memory"] =''

        try:
            item["internal"] = soup.find("td", text=re.compile("Internal")).find_next_sibling("td").get_text().strip()
        except:
            item["internal"] =''

        try:
            item["user_memory"] = soup.find("td", text=re.compile("User Memory")).find_next_sibling("td").get_text().strip()
        except:
            item["user_memory"] =''

        try:
            item["operating_freq"]=soup.find("td", text=re.compile("Operating Freq")).find_next_sibling("td").get_text().strip()
        except:
            item["operating_freq"] = ''

        try:
            item["user_interface"]=soup.find("td", text=re.compile("User Interface")).find_next_sibling("td").get_text().strip()
        except:
            item["user_interface"] =''

        try:
            item["graphics"] = soup.find("td", text=re.compile("Graphics")).find_next_sibling("td").get_text().strip()
        except:
            item["graphics"] =''

        try:
            item["os"] = soup.find("td", text=re.compile("OS")).find_next_sibling("td").get_text().strip()
        except:
            item["os"] =''

        try:
            item["processor"] = soup.find("td", text=re.compile("Processor")).find_next_sibling("td").get_text().strip()
        except:
            item["processor"] =''

        try:
            item["business_features"] = soup.find("td", text=re.compile("Business Features")).find_next("td").get_text().strip()
        except:
            item["business_features"] =''

        try:
            item["nfc"] = soup.find("td", text=re.compile("NFC")).find_next_sibling("td").get_text().strip()
        except:
            item["nfc"] =''
        yield item
