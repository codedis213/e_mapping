import scrapy

from tutorial.items import DmozItem, ComparerajaMobile
from bs4 import BeautifulSoup
import re
import json

class ComparerajaMobilesSpider(scrapy.Spider):
    name = "compareraja_mobiles"
    allowed_domains = ["compareraja.in"]

    start_urls = [
        "http://www.compareraja.in/mobiles.html",
    ]

    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        totalcount_tag = soup.find("p", {"id":"Total_Count"})
        totalcount = int(totalcount_tag.get_text().split()[-2].strip())
        rotational_step = totalcount / 20

        for index in range(1, rotational_step+1):
            rotation_link = 'http://www.compareraja.in/filter/controlls/commonfinderext.aspx?page='+str(index)+'&&Brand=&OS=&ScreenSize=&Camera=&Feature=&InternalStorage=&RAM=&minPrice=500&maxPrice=140000&sort=Popularity&categoryId=8&CategoryNameInURLs=mobiles&p_csv_Filter_01_Ids=&p_csv_Filter_03_Ids=&p_csv_Filter_04_Ids=&p_csv_Filter_05_Ids=&p_csv_Filter_06_Ids=&p_csv_Filter_07_Ids=&p_csv_Filter_08_Ids=&catid=8&catname=mobiles'
            yield scrapy.Request(rotation_link, callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        form_tag = soup2.find("form",{"id":"form1"})
        all_article_div = form_tag.find_all("article", {"class":"product"})

        for article_div in all_article_div:
            article_url = article_div.find("a").get("href")
            # item = ComparerajaMobile() # http://www.compareraja.in/htc-desire-626g-plus-dual-sim-price.html
            # item["article_link"] = article_url
            # yield item
            yield scrapy.Request(article_url, callback=self.parse_pro_link)

    def parse_pro_link(self, response):
        soup3 = BeautifulSoup(response.body, "html.parser")
        item = ComparerajaMobile()
        item["article_link"] = response.url
        item["error_link"] = True
        item["product_name"] = soup3.find("h1", {"itemprop":"name"}).get_text().strip()
        item["product_sku"] = soup3.find("meta", {"itemprop":"sku"}).get("content").strip()
        product_buy_link = soup3.find("div", {"class":"mer-box1"}).find("a", {"class":"prd-link"}).get("onclick")
        start_point = product_buy_link.find("('")
        end_point = product_buy_link.find("',", start_point)
        product_buy_link_str = product_buy_link[start_point+2:end_point]
        item["product_buy_link"] = product_buy_link_str.replace('&site=compareraja', "")\
            .replace("&affid=compareraj&affExtParam1=cmraja#LID#", "")\
            .replace("http://rover.ebay.com/rover/1/4686-127726-2357-26/2?&mpre=", "")
        item["product_domain_img"] = soup3.find("figure", {"class":"mer-img"}).find("img").get("src")
        product_price = soup3.find("p", {"class":"mer-text"}).get_text().strip()
        item["product_price"] = "".join([ch for ch  in product_price if ch.isdigit() or str(ch) == "."]).strip().strip(".")
        item["product_score"] = soup3.find("p", {"class":"prscr"}).get_text()

        all_img_div = soup3.find_all("p",{"class":"nsml"})
        item["product_img_list"] = [img_div.find("img").get("src") for img_div in all_img_div]

        compare_keys = ["Network", "MicroSIM", "NanoSIM", "DualSIM", "Touchscreen", "Dimension", "Weight", "Type", "ScreenSize",
                        "ScreenResolution", "DisplayType", "InternalMemory", "ExternalMemory", "Ram", "PrimaryCamera",
                        "SecondaryCamera", "PrimaryVideoQuality", "SecondaryVideoQuality", "Flash", "WIFI", "Radio", "GPRS",
                        "EDGE", "NFC", "USB", "BluetoothVersion", "OS", "OSVersion", "ProcessorType", "ProcessorSpeed",
                        "Chipset", "GPU", "BatteryType", "BatteryCapacity", "mmJack", "Loudspeaker"]

        item["Network"] = item["MicroSIM"] = item["NanoSIM"] = item["DualSIM"] = item["Touchscreen"] = item["Dimension"] = ''
        item["Weight"] = item["Type"] = item["ScreenSize"] = item["ScreenResolution"] = item["DisplayType"] = item["InternalMemory"] = ''
        item["ExternalMemory"] = item["Ram"] = item["PrimaryCamera"] = item["SecondaryCamera"] = item["PrimaryVideoQuality"] = item["SecondaryVideoQuality"] = ''
        item["Flash"] = item["WIFI"] = item["Radio"] = item["GPRS"] = item["EDGE"] = item["NFC"] = ''
        item["USB"] = item["BluetoothVersion"] = item["OS"] = item["OSVersion"] = item["ProcessorType"] = item["ProcessorSpeed"] = ''
        item["Chipset"] = item["GPU"] = item["BatteryType"] = item["BatteryCapacity"] = item["mmJack"] = item["Loudspeaker"] = ''

        features_all_ul_div = soup3.find("article", {"id":"Features"}).find_all("ul", {"class":"fedet"})

        for features_ul in features_all_ul_div:
            all_li_divs = features_ul.find_all("li")[1:]
            for li in all_li_divs:
                features_key = "".join(str(li.find("p").get_text()).strip().split())
                features_key_main = ''
                if features_key in compare_keys:
                    features_key_main   =  features_key
                elif features_key == "3.5mmJack":
                    features_key_main = "mmJack"
                elif features_key == "Touch-screen":
                    features_key_main = "Touchscreen"

                if features_key_main:
                    item[features_key_main] = li.find("p").find_next_sibling("span").get_text()

        yield item

