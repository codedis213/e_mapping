# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class DmozItem(scrapy.Item):
    domain = scrapy.Field()
    product_type = scrapy.Field()
    product_link = scrapy.Field()
    name = scrapy.Field()
    sub_name = scrapy.Field()
    rating = scrapy.Field()
    image_list = scrapy.Field()
    price = scrapy.Field()
    product_emi = scrapy.Field()
    availability = scrapy.Field()
    other_colors = scrapy.Field()
    in_the_box = scrapy.Field()
    # General features
    brand = scrapy.Field()
    handset_color = scrapy.Field()
    form = scrapy.Field()
    sim_size = scrapy.Field()
    call_features = scrapy.Field()
    model_name = scrapy.Field()
    touch_screen = scrapy.Field()
    sim_type = scrapy.Field()
    model_id = scrapy.Field()
    video_calling = scrapy.Field()
    # Multimedia
    sound_enhancement = scrapy.Field()
    video_player = scrapy.Field()
    fm  = scrapy.Field()
    music_player = scrapy.Field()
    # Camera
    video_recording = scrapy.Field()
    flash = scrapy.Field()
    other_camera_features = scrapy.Field()
    hd_recording = scrapy.Field()
    rear_camera = scrapy.Field()
    front_facing_camera = scrapy.Field()
    zoom = scrapy.Field()
    # Internet & connectivity
    audio_jack = scrapy.Field()
    preinstalled_browser = scrapy.Field()
    bluetooth = scrapy.Field()
    fourg = scrapy.Field()
    navigation_technology = scrapy.Field()
    wifi = scrapy.Field()
    internet_features = scrapy.Field()
    edeg = scrapy.Field()
    gprs = scrapy.Field()
    tethering = scrapy.Field()
    usb_connectivity = scrapy.Field()
    threeg = scrapy.Field()
    # Other features
    sensors = scrapy.Field()
    call_memory = scrapy.Field()
    sar_value = scrapy.Field()
    important_apps = scrapy.Field()
    additional_features = scrapy.Field()
    phone_book_memory = scrapy.Field()
    sms_memory = scrapy.Field()
    # Dimensions
    # weight = scrapy.Field()
    # dimensions_size = scrapy.Field()
    dimensions = scrapy.Field()
    # Display
    resolution = scrapy.Field()
    other_display_features = scrapy.Field()
    color = scrapy.Field()
    size = scrapy.Field()
    # Warranty
    warranty_summary = scrapy.Field()
    # Battery
    removable_battery = scrapy.Field()
    battery_type = scrapy.Field()
    # Memory and storage
    expandable_memory = scrapy.Field()
    memory = scrapy.Field()
    internal = scrapy.Field()
    user_memory = scrapy.Field()
    # Platform
    operating_freq = scrapy.Field()
    user_interface = scrapy.Field()
    graphics = scrapy.Field()
    os = scrapy.Field()
    processor = scrapy.Field()
    business_features = scrapy.Field()
    nfc = scrapy.Field()












class ComparerajaMobile(scrapy.Item):
    error_link = scrapy.Field()
    article_link = scrapy.Field()
    product_name = scrapy.Field()
    product_sku = scrapy.Field()
    product_buy_link = scrapy.Field()
    product_domain_img = scrapy.Field()
    product_price = scrapy.Field()
    product_score = scrapy.Field()
    product_img_list = scrapy.Field()
    Network = scrapy.Field()
    MicroSIM = scrapy.Field()
    NanoSIM = scrapy.Field()
    DualSIM = scrapy.Field()
    Touchscreen = scrapy.Field()
    Dimension = scrapy.Field()
    Weight = scrapy.Field()
    Type = scrapy.Field()
    ScreenSize = scrapy.Field()
    ScreenResolution = scrapy.Field()
    DisplayType = scrapy.Field()
    InternalMemory = scrapy.Field()
    ExternalMemory = scrapy.Field()
    Ram = scrapy.Field()
    PrimaryCamera = scrapy.Field()
    SecondaryCamera = scrapy.Field()
    PrimaryVideoQuality = scrapy.Field()
    SecondaryVideoQuality = scrapy.Field()
    Flash = scrapy.Field()
    WIFI = scrapy.Field()
    Radio = scrapy.Field()
    GPRS = scrapy.Field()
    EDGE = scrapy.Field()
    NFC = scrapy.Field()
    USB = scrapy.Field()
    BluetoothVersion = scrapy.Field()
    OS = scrapy.Field()
    OSVersion = scrapy.Field()
    ProcessorType = scrapy.Field()
    ProcessorSpeed = scrapy.Field()
    Chipset = scrapy.Field()
    GPU = scrapy.Field()
    BatteryType = scrapy.Field()
    BatteryCapacity = scrapy.Field()
    mmJack = scrapy.Field()
    Loudspeaker = scrapy.Field()






