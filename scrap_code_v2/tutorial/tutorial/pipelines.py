# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class TutorialPipeline(object):
    def process_item(self, item, spider):
        return item


from twisted.enterprise import adbapi
from scrapy.utils.project import get_project_settings

settings = get_project_settings()


# class FundPipeline(object):
#     # The table you items.FundItem class map to, my table is named fund
#     # insert_sql = """insert into polls_dmozitem (%s) values ( %s )"""
#
#     insert_sql = """insert into polls_dmozitem (%s) values ( %s )"""
#
#     def __init__(self):
#         dbargs = settings.get('DB_CONNECT')
#         db_server = settings.get('DB_SERVER')
#         dbpool = adbapi.ConnectionPool(db_server, **dbargs)
#         self.dbpool = dbpool
#
#
#     def __del__(self):
#         self.dbpool.close()
#
#     def process_item(self, item, spider):
#         self.insert_data(item, self.insert_sql)
#         return item
#
#     def insert_data(self, item, insert):
#         keys = item.fields.keys()
#         fields = u','.join(keys)
#         qm = u','.join([u'%s'] * len(keys))
#         sql = insert % (fields, qm)
#         data = [str(item[k]).strip() for k in keys]
#         return self.dbpool.runOperation(sql, data)


# Cannot use this to create the table, must have table already created

from twisted.enterprise import adbapi
import datetime
import MySQLdb.cursors
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

# class FundPipeline(object):
#
#     def __init__(self):
#         self.dbpool = adbapi.ConnectionPool('MySQLdb', db='e_map_v2',
#         user='root', passwd='root', cursorclass=MySQLdb.cursors.DictCursor,
#         charset='utf8', use_unicode=True)
#
#     def process_item(self, item, spider):
#         query = self.dbpool.runInteraction(self._conditional_insert, item)
#         query.addErrback(self.handle_error)
#
#         return item
#
#     def _conditional_insert(self, tx, item):
#         sql_condition_stmnt = """select * from polls_dmozitem where product_type  = '%s'
#                                 and brand = '%s' and model_id like '%s%s'"""
#         sql_condition_stmnt = sql_condition_stmnt %(item["product_type"], item["brand"], item["model_id"], "%")
#
#         tx.execute(sql_condition_stmnt)
#         result = tx.fetchone()
#
#         logging.debug("Item ready to do operation in db: %s" % item)
#
#         if result:
#             # result[7] == result["price"]
#             if result[7] > item["price"]:
#                 update_sql = """update polls_dmozitem set domain = "%s", product_link = "%s", price ="%s" product_emi="%s"
#                                 where product_type  = '%s' and brand = '%s' and model_id like '"""+ item["model_id"] + """%'
#                                 """
#                 update_sql = update_sql %(item["domain"], item["product_link"], item["price"], item["product_emi"],
#                                       item["product_type"], item["brand"])
#
#                 tx.execute(update_sql)
#                 logging.debug("Item already stored in db: %s" % item)
#
#         else:
#             insert_sql = """insert into polls_dmozitem (%s) values ( %s )"""
#             keys = item.fields.keys()
#             fields = u','.join(keys)
#             qm = u','.join([u'"%s"'] * len(keys))
#             sql = insert_sql % (fields, qm)
#             data = [str(item[k]).strip() for k in keys]
#             sql = sql % tuple(data)
#             tx.execute(sql)
#
#             logging.debug("Item stored in db: %s" % item)
#
#     def handle_error(self, e):
#         logging.debug(e)

class FundPipeline(object):

    def __init__(self):
        self.dbpool = adbapi.ConnectionPool('MySQLdb', db='e_map_v2',
        user='root', passwd='root', cursorclass=MySQLdb.cursors.DictCursor,
        charset='utf8', use_unicode=True)

    def process_item(self, item, spider):
        query = self.dbpool.runInteraction(self._conditional_insert, item)
        query.addErrback(self.handle_error)

        return item

    def _conditional_insert(self, tx, item):
        sql_condition_stmnt = """select * from polls_dmozitem where product_type  = '%s'
                                and brand = '%s'  and product_link = "%s" and model_id like '%s%s'"""
        sql_condition_stmnt = sql_condition_stmnt %(item["product_type"], item["brand"], item["product_link"],
                                                    item["model_id"], "%")

        tx.execute(sql_condition_stmnt)
        result = tx.fetchone()

        logging.debug("Item ready to do operation in db: %s" % item)

        keys = item.fields.keys()
        fields = u','.join(keys)
        keys_match_str = u' = "%s", '.join(keys) + ' = "%s" '
        qm = u','.join([u'"%s"'] * len(keys))
        data = [str(item[k]).lower().strip() for k in keys]

        if result:
            update_sql = """update polls_dmozitem set """
            update_sql += keys_match_str

            update_sql += """where id = """+ str(result["id"])

            update_sql = update_sql % tuple(data)

            tx.execute(update_sql)
            logging.debug("Item already stored in db: %s" % item)

        else:
            insert_sql = """insert into polls_dmozitem (%s) values ( %s )"""

            sql = insert_sql % (fields, qm)
            sql = sql % tuple(data)
            tx.execute(sql)

            logging.debug("Item stored in db: %s" % item)

    def handle_error(self, e):
        logging.debug(e)